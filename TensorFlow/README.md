Description
=======
TensorFlow is an end-to-end open source platform for machine learning.

Homepage
--------
[www.tensorflow.org](https://www.tensorflow.org)

Version
-------

GCC/10.3.0  OpenMPI/4.1.1 TensorFlow/2.6.0-CUDA-11.3.1

Dependencies
------------
[tensorflow-model-optimization](https://github.com/tensorflow/model-optimization)
[gsutil](https://pypi.org/project/gsutil/)
[google-cloud-bigquery](https://pypi.org/project/google-cloud-bigquery/)

Compilation
-----------
You need to use GCC compiler version 10 or bigger with openMPI and CUDA support


Benchmark
---------
The benchmark framework used is available [here](https://github.com/tensorflow/benchmarks/tree/master/perfzero)

As this framework doesn't have release tags, we are using the commit "16af178ad312e8c1213efb27a5f227044228bfdf".

Download the benchmark: 

```
wget https://github.com/tensorflow/benchmarks/archive/16af178ad312e8c1213efb27a5f227044228bfdf.tar.gz -O tf_benchmarks.tar.gz
tar xf tf_benchmarks.tar.gz
```


Data to run the benchmarks
--------------------------
The CIFAR-10 dataset can be downloaded [here](https://www.cs.toronto.edu/~kriz/cifar-10-binary.tar.gz)


Running
-------

```
sbatch runTFBench.sh
```

This run produces errors as the framework is tring to upload the result to the cloud. This error is not relevant.
   
Results
---------
The RAW value is the total images/sec. \
Please provide the full output log

