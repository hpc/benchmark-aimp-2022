#!/bin/sh
#SBATCH --time=10:00
#SBATCH --gpus=volta:1
#SBATCH --partition=shared-gpu
#SBATCH --cpus-per-task=1
#SBATCH --mem=12G

ml GCC/10.3.0  OpenMPI/4.1.1 TensorFlow/2.6.0-CUDA-11.3.1 git/2.32.0-nodocs tensorflow-model-optimization/0.7.1-CUDA-11.3.1

export ROOT_DATA_DIR=/home/sagon/data/
export MODEL=official.benchmark.keras_cifar_benchmark.Resnet56KerasBenchmarkReal.benchmark_1_gpu_no_dist_strat

echo NTASKS=$SLURM_NTASKS
echo SLURM_NODELIST=$SLURM_NODELIST
echo SLURM_NNODES=$SLURM_NNODES
echo MEM=$MEM
echo MODEL=$MODEL

srun python benchmarks-16af178ad312e8c1213efb27a5f227044228bfdf/perfzero/lib/benchmark.py \
--git_repos="https://github.com/tensorflow/models.git;benchmark" \
--python_path=models \
--root_data_dir=$ROOT_DATA_DIR \
--benchmark_methods=$MODEL \
--gcloud_key_file_url="" 
