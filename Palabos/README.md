Description
===========
Palabos is an open-source CFD solver based on the lattice Boltzmann method.

Homepage
--------
[www.palabos.org](https://palabos.unige.ch/)

Compilation
-----------
You need to use GCC compiler version 10 or bigger with openMPI support.
   
    wget https://gitlab.com/unigespc/palabos/-/archive/v2.3.0/palabos-v2.3.0.tar.gz
    tar xvf palabos-v2.3.0.tar.gz
    pushd palabos-v2.3.0/examples/benchmarks/cavity3d/build
    module load foss/2021b CMake
    cmake ..
    make -j 12
    popd

Running
-------
The benchmark will run in about ~2 minutes. You must run it as a SLURM batch job.

1. Run on one core \
   N = 700 \
   We want to measure the quality of one core

2. Run on 512 cores using openMPI \
   N = 700 \ 
   We want to measure the interconnect quality

2. Run on 800 cores using openMPI \
   N = 2500 \ 
   Real use case \

3. Test on a full node using openMPI \ 
   N = 700 \
   We want to measure the quality of the shared memory

```
sbatch runPalabosCavity3d.sh
```
   
Results
---------
Output sample:

    Starting benchmark with 201x201x201 grid points (approx. 2 minutes on modern processors).
    Number of MPI threads: 16
    After 591 iterations: 96.0463 Mega site updates per second.
