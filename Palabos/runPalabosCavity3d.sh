#!/bin/sh
#SBATCH --job-name=test-palabos
#SBATCH --partition=shared-cpu
#SBATCH --constraint=AMD_EPYC_7742
#SBATCH --cpus-per-task=1
#SBATCH --ntasks=512
#SBATCH --mem=240G
#SBATCH --time=6:00

module load foss/2021b

# resolution http://wiki.palabos.org/plb_wiki:benchmarks
N=700

echo N=$N
echo NTASKS=$SLURM_NTASKS
echo SLURM_NODELIST=$SLURM_NODELIST
echo SLURM_NNODES=$SLURM_NNODES
echo MEM=$MEM

srun palabos-v2.3.0/examples/benchmarks/cavity3d/cavity3d $N
