#!/bin/sh
#SBATCH --job-name=test-hpl
#SBATCH --cpus-per-task=1
#SBATCH --ntasks=800
#SBATCH --partition=shared-cpu
#SBATCH --constraint=XEON_GOLD_6240
#SBATCH --time=12:00:00

## minimum 800 tasks

ml GCC/11.2.0  OpenMPI/4.1.1 HPL/2.3

echo NTASKS=$SLURM_NTASKS
echo SLURM_NODELIST=$SLURM_NODELIST
echo SLURM_NNODES=$SLURM_NNODES

srun xhpl 
