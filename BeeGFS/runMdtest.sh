#!/bin/sh
#SBATCH --partition=shared-cpu
#SBATCH --time=10:00:00
#SBATCH --ntasks=128
#SBATCH --constraint=AMD_EPYC_7742
#SBATCH --cpus-per-task=1
#SBATCH --exclusive
#SBATCH --nodes=4
#SBATCH --mem=240G
#SBATCH --output=slurm-mdtest-128-task-four-nodes-files-per-dir-122-%j.out

## output files
## slurm-mdtest-one-task-one-node-files-per-dir-15625-%j.out
## slurm-mdtest-128-task-one-node-files-per-dir-122-%j.out
## slurm-mdtest-128-task-four-nodes-files-per-dir-122-%j.out

module load GCC/10.3.0  OpenMPI/4.1.1 IOR/3.3.0

# ${NUM_DIRS} = (parameter -b ^ parameter -z)
# -b 8
# -z 2
# NUM_DIRS = 64
#
# Compute FILES_PER_DIR
# The total amount of files should always be higher than 1 000 000, so ${FILES_PER_DIR} is calculated as ${FILES_PER_DIR} = (1000000 / ${NUM_DIRS} / ${NUM_PROCS}).
# Example: FILES_PER_DIR = (1000000 / 64 / 8) = 1954


#DIRNAME=/srv/beegfs/scratch/users/s/sagon/mdtest
DIRNAME=/home/sagon/mdtest

FILES_PER_DIR=122

ITERATIONS=2

echo NTASKS=$SLURM_NTASKS
echo SLURM_NODELIST=$SLURM_NODELIST
echo SLURM_NNODES=$SLURM_NNODES
echo ITERATIONS=$ITERATIONS
echo FILES_PER_DIR=$FILES_PER_DIR
echo DIRNAME=$DIRNAME


# be sure that the DIRNAME is empty. Mdtest should take care of removing the data when he completes successfuly.
#if [ -d $DIRNAME ]; then
#  rm -rf $DIRNAME
#fi

srun mdtest -C -T -r -F -d ${DIRNAME} -i ${ITERATIONS} -I ${FILES_PER_DIR} -z 2 -b 8 -L -u

