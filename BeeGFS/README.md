Description
===========
We want to perform some BeeGFS benchmarks, using IOR and BeeGFS itself.

Homepage
--------
https://doc.beegfs.io/latest/advanced_topics/benchmark.html

Compilation
-----------
You need to use GCC compiler version 10 or higher with openMPI support.

Version
-------
IOR version 3.3.0
If you use another version, please specify it.


BeeGFS storagebench
===================

Write test
----------

    beegfs-ctl --storagebench --alltargets --write --blocksize=128K --size=300G --threads=20

Read test
----------

    beegfs-ctl --storagebench --alltargets --read --blocksize=4K --size=300G --threads=20

Get results
----------

    beegfs-ctl --storagebench --alltargets --status

Cleanup files
-------------

    beegfs-ctl --storagebench --alltargets --cleanup


IOR
===

Storage
-------
This test measure the read/write performance of the storage target through the network.

We want to perform a read/write test. Once on a separate file per process and once on a shared file (option -F)
The blocksize should be 10G (!!! ?? !!!)

The included [slurm sbatch file](runIOR.sh) will run both tests. All IOR defined parameters must be kept except transfer size that can be adjusted. Feel free to adapt the tunning parameters related to BeeGFS.
The test should be launched three time with the following SLURM parameters:

1. number of nodes: 1, number of tasks: 1, block size: 1152G
2. number of nodes: 1, number of tasks: 64, block size: 18G
3. number of nodes: 8, number of tasks: 64 (ie. 8 tasks per nodes), block size: 18G


mdtest
======
This test measure the metada operations performances on files and directories. We want to test operations on circa 1M files written in 8 x 8  sub-directories.

The included [sbatch](runMdtest.sh) will run the test. All mdtest defined parameters must be kept.

The test should be launched three time with the following SLURM parameters:

1. number of nodes: 1, number of tasks: 1, number of files per directory: 15625
2. number of nodes: 1, number of tasks: 20 (or total number of cores of a compute node), number of files per directory: 782
3. number of nodes: 10, number of tasks: 40 (ie. 4 tasks per nodes), number of files per directory: 390


Running
-------
The IOR and mdtest must be run through SLURM batch jobs with the scripts described above.

   
Results
-------
We want the full log of each run.
