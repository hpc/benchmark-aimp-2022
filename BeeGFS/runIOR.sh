#!/bin/sh
#SBATCH --partition=shared-cpu
#SBATCH --time=01:30:00
#SBATCH --output=slurm-ior-one-task-one-node-bs-1152G-%j.out
#SBATCH --constraint=AMD_EPYC_7742
#SBATCH --ntasks=64
#SBATCH --cpus-per-task=1
#SBATCH --nodes=8
#SBATCH --exclusive

## output files:
## slurm-ior-one-task-one-node-bs-1152G-%j.out
## slurm-ior-64-tasks-8-nodes-bs-18G-%j.out
## slurm-ior-64-task-one-node-bs-18G-%j.out
## slurm-ior-128-task-four-node-bs-9G-%j.out

module load GCC/10.3.0 OpenMPI/4.1.1 IOR/3.3.0

# 3 * RAM_SIZE_PER_STORAGE_SERVER * NUM_STORAGE_SERVERS) / ${NUM_PROCS})
# For example with ntasks = 1:
# 3 * 192G * 2 / 1 = 1152G

BLOCK_SIZE=18G
ITERATIONS=1
OUTPUT_FILE=/home/sagon/${SLURM_JOB_ID}.ior

echo BLOCK_SIZE=$BLOCK_SIZE
echo ITERATIONS=$ITERATIONS
echo NTASKS=$SLURM_NTASKS
echo SLURM_NODELIST=$SLURM_NODELIST
echo SLURM_NNODES=$SLURM_NNODES


# write on a separate file per process
srun ior -r -w -i${ITERATIONS} -t2m -b ${BLOCK_SIZE} -g -F -e -o $OUTPUT_FILE


# write on the same file per process
srun ior -r -w -i${ITERATIONS} -t2m -b ${BLOCK_SIZE} -g  -e -o $OUTPUT_FILE
