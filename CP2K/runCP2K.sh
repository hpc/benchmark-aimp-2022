#!/bin/sh
#SBATCH --job-name=test-cp2k
#SBATCH --cpus-per-task=1
#SBATCH --tasks=128
#SBATCH --partition=shared-cpu
#SBATCH --time=1:00:00
#SBATCH --constraint=AMD_EPYC_7742
#SBATCH --mem=240G

ml GCC/10.2.0  OpenMPI/4.0.5 CP2K/8.1


# *** WARNING in dbcsr/mm/dbcsr_mm.F:268 :: Using a non-square number of ***
# *** MPI ranks might lead to poor performance.                          ***
# *** Used ranks: 300                                                    ***
# *** Suggested: 289 576                                                 ***

VARIANT=H2O-512.inp

BASE_INPUT=cp2k-8.1.0/benchmarks/QS/

INPUT=$BASE_INPUT/$VARIANT
OUTPUT=cp2k-${SLURM_NTASKS}-tasks-${VARIANT}-$SLURM_JOB_ID.out

echo INPUT=$INPUT
echo NTASKS=$SLURM_NTASKS
echo SLURM_NODELIST=$SLURM_NODELIST
echo SLURM_NNODES=$SLURM_NNODES
echo JOBID=$SLURM_JOB_ID
echo MEM=$MEM

srun -o $OUTPUT cp2k.popt -i $INPUT
